import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SidenavService } from './modules/layout/services';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, Observable } from 'rxjs';
import { MatDrawerMode } from '@angular/material/sidenav';
import { gql } from 'apollo-angular';

const HELLO_QUERY = gql`
  query MangaGetById($id: ID!) {
    mangaGet(id: $id) {
      id
      title
      titleSlug

      tags {
        id
        name
        nameSlug
      }
    }
  }
`

@Component({
  selector: 'moe-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

  public readonly sidenavMode$: Observable<MatDrawerMode>;

  constructor(
    public readonly sidenav: SidenavService,
    private readonly _breakpointObserver: BreakpointObserver
  ) {
    this.sidenavMode$ = this._breakpointObserver
      .observe(Breakpoints.XSmall)
      .pipe(map(r => r.matches ? 'over' : 'side'));
  }

}
