import { TestBed } from '@angular/core/testing';

import { Theme, THEME_STORAGE, ThemeService, ThemeStorage } from './theme.service';
import { firstValueFrom } from 'rxjs';

describe('ThemeService', () => {
  let savedTheme: Theme | null = null;
  let storageSpy: jasmine.SpyObj<ThemeStorage>;

  beforeEach(() => {
    savedTheme = null;

    storageSpy = jasmine.createSpyObj<ThemeStorage>(['set', 'get']);
    storageSpy.set.and.callFake((val) => savedTheme = val);
    storageSpy.get.and.callFake(() => savedTheme);

    TestBed.configureTestingModule({
      providers: [
        { provide: THEME_STORAGE, useValue: storageSpy }
      ]
    });
  });

  it('should be created', () => {
    let service = TestBed.inject(ThemeService);
    expect(service).toBeTruthy();
  });

  it('should default to dark', async () => {
    let service = TestBed.inject(ThemeService);

    const theme = await firstValueFrom(service.themeChanges$);

    expect(theme).toEqual('dark');
  });

  it('should init persisted theme', async () => {
    savedTheme = 'light';

    let service = TestBed.inject(ThemeService);
    service.initPersistedTheme();

    const theme = await firstValueFrom(service.themeChanges$);

    expect(theme).toEqual('light');
  });

  it('should be able to change theme', async () => {
    let service = TestBed.inject(ThemeService);

    service.setLight();
    const firstValue = await firstValueFrom(service.themeChanges$);
    expect(firstValue).toEqual('light');

    service.setDark();
    const secondValue = await firstValueFrom(service.themeChanges$);
    expect(secondValue).toEqual('dark');
  });

  it('should persist theme upon change', async () => {
    let service = TestBed.inject(ThemeService);

    service.setLight();
    expect(storageSpy.set).toHaveBeenCalledWith('light');

    service.setDark();
    expect(storageSpy.set).toHaveBeenCalledWith('dark');
  });
});
