import { Inject, Injectable, InjectionToken, OnDestroy, Optional } from '@angular/core';
import { BehaviorSubject, Observable, Subject, takeUntil } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

export interface LanguageInfo {
  code: string;
  icon: string;
  fullName: string;
}

export type AvailableLanguages = ReadonlyMap<string, LanguageInfo>;

export interface LanguageStorage {
  get(): string | null;

  set(lang: string): void;
}

export const LANGUAGE_STORAGE = new InjectionToken<LanguageStorage>('Language storage');
export const AVAILABLE_LANGUAGES = new InjectionToken<AvailableLanguages>('Available languages');
export const PREFERRED_LANGUAGE = new InjectionToken<string | null>('Preferred language');

@Injectable({
  providedIn: 'root'
})
export class LanguageService implements OnDestroy {

  private readonly _currentLanguage = new BehaviorSubject(this._translateService.defaultLang);
  public readonly currentLanguage$: Observable<string> = this._currentLanguage.asObservable();

  public get currentLanguage(): string {
    return this._currentLanguage.value;
  }

  public set currentLanguage(lang: string) {
    if (this.availableLanguages.get(lang) == null) {
      throw new Error(`Language doesn't exist`);
    }
    this._translateService.use(lang);
  }

  public get availableLanguages(): AvailableLanguages {
    return this._availableLanguages;
  };

  private readonly _destroyed = new Subject<void>();

  constructor(
    private readonly _translateService: TranslateService,
    @Inject(LANGUAGE_STORAGE) private readonly _languageStorage: LanguageStorage,
    @Inject(AVAILABLE_LANGUAGES) private readonly _availableLanguages: AvailableLanguages,
    @Optional() @Inject(PREFERRED_LANGUAGE) private readonly _preferredLanguage: string | null,
  ) {
    this._translateService.onLangChange
      .pipe(takeUntil(this._destroyed))
      .subscribe(c => {
        this._currentLanguage.next(c.lang);
        this._languageStorage.set(c.lang);
      });
  }

  public ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }

  public initPersistedLang(): void {
    this.currentLanguage = this._languageStorage.get()
      ?? this._preferredLanguage
      ?? this._translateService.defaultLang;
  }
}
