import { TestBed } from '@angular/core/testing';

import { AVAILABLE_LANGUAGES, LANGUAGE_STORAGE, LanguageService, LanguageStorage, PREFERRED_LANGUAGE } from './language.service';
import { LangChangeEvent, TranslateModule, TranslateService } from '@ngx-translate/core';
import { EventEmitter } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';

describe('LanguageService', () => {
  let savedLang: string | null = null;

  let storageSpy: jasmine.SpyObj<LanguageStorage>;
  let translateServiceSpy: jasmine.SpyObj<TranslateService>;

  beforeEach(() => {
    savedLang = null;

    storageSpy = jasmine.createSpyObj<LanguageStorage>(['get', 'set']);
    storageSpy.set.and.callFake((val) => savedLang = val);
    storageSpy.get.and.callFake(() => savedLang);

    const langChange = new EventEmitter<LangChangeEvent>();
    translateServiceSpy = jasmine.createSpyObj<TranslateService>(
      ['defaultLang', 'use'],
      {
        get defaultLang(): string {
          return 'a'
        },
        use(lang: string): Observable<void> {
          langChange.next({ lang, translations: null });
          return EMPTY;
        },
        get onLangChange(): EventEmitter<LangChangeEvent> {
          return langChange;
        }
      });

    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        { provide: LANGUAGE_STORAGE, useValue: storageSpy },
        { provide: TranslateService, useValue: translateServiceSpy },
        {
          provide: AVAILABLE_LANGUAGES,
          useValue: new Map([
            ['a', { code: 'a', icon: 'a', fullName: 'a' }],
            ['b', { code: 'b', icon: 'b', fullName: 'b' }],
          ])
        }
      ]
    });
  });

  it('should be created', () => {
    let service = TestBed.inject(LanguageService);
    expect(service).toBeTruthy();
  });

  it('should init persisted from store', () => {
    storageSpy.set('b');

    let service = TestBed.inject(LanguageService);
    service.initPersistedLang();

    expect(service.currentLanguage).toEqual('b');
  });

  it('should init with preferred language', () => {
    TestBed.overrideProvider(PREFERRED_LANGUAGE, { useValue: 'b' });

    let service = TestBed.inject(LanguageService);
    service.initPersistedLang();

    expect(service.currentLanguage).toEqual('b');
  });

  it('should init with default language', () => {
    let service = TestBed.inject(LanguageService);

    service.initPersistedLang();

    expect(service.currentLanguage).toEqual('a');
  });

  it('should set only available language', () => {
    let service = TestBed.inject(LanguageService);

    expect(() => service.currentLanguage = 'abc').toThrow();
  });
});
