import { Inject, Injectable, InjectionToken, Renderer2, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

export type Theme = 'light' | 'dark';

export interface ThemeStorage {
  get(): Theme | null;

  set(theme: Theme): void;
}

export const THEME_STORAGE = new InjectionToken<ThemeStorage>('Theme storage');

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private readonly _theme = new BehaviorSubject<Theme>('dark');
  public readonly themeChanges$ = this._theme.asObservable();

  private readonly _renderer: Renderer2;

  constructor(
    @Inject(DOCUMENT) private readonly _document: Document,
    @Inject(THEME_STORAGE) private readonly _themeStorage: ThemeStorage,
    rendererFactory: RendererFactory2,
  ) {
    this._renderer = rendererFactory.createRenderer(null, null);
  }

  public toggle(): void {
    if (this._theme.value == 'light') {
      this.setDark();
    } else {
      this.setLight();
    }
  }

  public setLight(): void {
    this._themeStorage.set('light');
    this._theme.next('light');
    this._renderer.addClass(this._document.body, 'light-theme');
  }

  public setDark(): void {
    this._themeStorage.set('dark');
    this._theme.next('dark');
    this._renderer.removeClass(this._document.body, 'light-theme');
  }

  public initPersistedTheme(): void {
    const theme = this._themeStorage.get();
    if (theme == null) {
      return;
    }

    if (theme == 'light') {
      this.setLight();
    } else {
      this.setDark();
    }
  }
}
