import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { RelativePathInterceptor, serverTranslateLoaderFactory } from './core/server';
import { TranslateLoader } from '@ngx-translate/core';
import { TransferState } from '@angular/platform-browser';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: RelativePathInterceptor, multi: true },
    {
      provide: TranslateLoader,
      useFactory: serverTranslateLoaderFactory,
      deps: [HttpClient, TransferState]
    }
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {
}
