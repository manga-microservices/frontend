import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsViewComponent, SettingComponent } from './components';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { UiLanguageSettingComponent } from './components/ui-language-setting/ui-language-setting.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
    SettingsViewComponent,
    SettingComponent,
    UiLanguageSettingComponent,
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,

    TranslateModule,

    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatCardModule
  ]
})
export class SettingsModule {
}
