import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsViewComponent } from './components';

const routes: Routes = [
  {
    path: '',
    title: 'ROUTE.SETTINGS',
    component: SettingsViewComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {
}
