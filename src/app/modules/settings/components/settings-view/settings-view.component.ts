import { ChangeDetectionStrategy, Component, OnInit, Type } from '@angular/core';
import { Location } from '@angular/common';
import { UiLanguageSettingComponent } from '../ui-language-setting/ui-language-setting.component';

interface Setting {
  titleKey: string;
  descriptionKey: string;
  component: Type<any>
}

@Component({
  selector: 'moe-settings-view',
  templateUrl: './settings-view.component.html',
  styleUrls: ['./settings-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsViewComponent implements OnInit {

  public readonly settings: readonly Setting[] = [
    {
      titleKey: 'SETTINGS.UI_LANGUAGE_SETTING.TITLE',
      descriptionKey: 'SETTINGS.UI_LANGUAGE_SETTING.DESCRIPTION',
      component: UiLanguageSettingComponent
    }
  ];

  constructor(
    public readonly location: Location
  ) {
  }

  public ngOnInit(): void {
  }

}
