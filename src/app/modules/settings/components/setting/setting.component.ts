import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'moe-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingComponent {

  @Input()
  public title: string = '';

  @Input()
  public description: string = '';

}
