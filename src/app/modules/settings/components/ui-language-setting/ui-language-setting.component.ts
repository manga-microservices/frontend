import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { LanguageService } from 'app/services';
import { FormControl } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'moe-ui-language-setting',
  templateUrl: './ui-language-setting.component.html',
  styleUrls: ['./ui-language-setting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UiLanguageSettingComponent implements OnInit, OnDestroy {

  public readonly languageControl = new FormControl<string>(
    this.languageService.currentLanguage,
    { nonNullable: true }
  );

  private readonly _destroyed = new Subject<void>();

  constructor(
    public readonly languageService: LanguageService
  ) {
  }

  public ngOnInit(): void {
    this.languageControl.valueChanges
      .pipe(takeUntil(this._destroyed))
      .subscribe(lang => this.languageService.currentLanguage = lang);
  }

  public ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }
}
