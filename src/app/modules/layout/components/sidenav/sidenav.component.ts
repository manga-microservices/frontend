import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SidenavService } from '../../services';

interface SidenavLink {
  icon: string;
  translateKey: string;
  path: string;
  exactMatch?: boolean;
}

@Component({
  selector: 'moe-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent {

  public readonly links: readonly SidenavLink[] = [
    { icon: 'home', translateKey: 'HOME', path: '', exactMatch: true },
    { icon: 'import_contacts', translateKey: 'MANGA', path: 'manga' },
    { icon: 'bookmarks', translateKey: 'BOOKMARKS', path: 'bookmarks' },
    { icon: 'groups', translateKey: 'GROUPS', path: 'groups' },
  ];

  constructor(
    public readonly sidenav: SidenavService
  ) {
  }
}
