import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SidenavService } from '../../services';
import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'moe-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent {

  @Input()
  get sticky(): boolean {
    return this._sticky;
  }

  set sticky(value: BooleanInput) {
    this._sticky = coerceBooleanProperty(value);
  }

  private _sticky: boolean = false;

  constructor(
    public readonly sidenav: SidenavService
  ) {
  }

}
