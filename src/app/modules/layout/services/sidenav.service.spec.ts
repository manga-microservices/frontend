import { TestBed } from '@angular/core/testing';

import { SidenavService } from './sidenav.service';
import { firstValueFrom } from 'rxjs';

describe('SidenavService', () => {
  let service: SidenavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidenavService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be closed by default', async () => {
    const value = await firstValueFrom(service.state$);
    expect(value).toBeFalse();
  });

  it('should open and close', async () => {
    service.open();
    const valueAfterOpened = await firstValueFrom(service.state$);
    expect(valueAfterOpened).toBeTrue();

    service.close();
    const valueAfterClosed = await firstValueFrom(service.state$);
    expect(valueAfterClosed).toBeFalse();
  });

  it('should toggle', async () => {
    const defaultValue = await firstValueFrom(service.state$);

    service.toggle();
    const valueAfterFirstToggle = await firstValueFrom(service.state$);
    expect(valueAfterFirstToggle).toEqual(!defaultValue);

    service.toggle();
    const valueAfterSecondToggle = await firstValueFrom(service.state$);
    expect(valueAfterSecondToggle).toEqual(!valueAfterFirstToggle);
  });
});
