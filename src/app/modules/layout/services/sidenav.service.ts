import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private readonly _state = new BehaviorSubject<boolean>(false);
  public readonly state$: Observable<boolean> = this._state.asObservable();

  public close(): void {
    this._state.next(false);
  }

  public open(): void {
    this._state.next(true);
  }

  public toggle(): void {
    const currentValue = this._state.value;
    this._state.next(!currentValue);
  }
}
