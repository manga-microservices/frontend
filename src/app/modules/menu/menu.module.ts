import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { MenuComponent, ThemeToggleComponent } from './components';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    TranslateModule,

    MatButtonModule,
    MatIconModule,
    OverlayModule,
    MatListModule,
  ],
  declarations: [ThemeToggleComponent, MenuComponent],
  exports: [MenuComponent]
})
export class MenuModule {
}
