import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { filter, Subject, takeUntil } from 'rxjs';

interface MenuItem {
  icon: string;
  translateKey: string;
  path: string;
}

@Component({
  selector: 'moe-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit, OnDestroy {

  public readonly menuItems: readonly MenuItem[] = [
    { icon: 'person', translateKey: 'PROFILE', path: 'profile' },
    { icon: 'bookmark', translateKey: 'FOLLOWS', path: 'follows' },
    { icon: 'group', translateKey: 'GROUPS', path: 'my-groups' },
  ];

  public menuOpen: boolean = false;

  private readonly _destroyed = new Subject<void>();

  constructor(
    private readonly _router: Router
  ) {
  }

  public ngOnInit(): void {
    this._router.events.pipe(
      filter(e => e instanceof NavigationStart && this.menuOpen),
      takeUntil(this._destroyed)
    ).subscribe(() => this.menuOpen = false);
  }

  public ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }
}
