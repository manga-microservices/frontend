import { ChangeDetectionStrategy, Component } from '@angular/core';
import { map, Observable } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ThemeService } from 'app/services';

type ThemeIcon = 'light_mode' | 'dark_mode';
type AnimationState = 'light' | 'dark';

@Component({
  selector: 'moe-theme-toggle',
  templateUrl: './theme-toggle.component.html',
  styleUrls: ['./theme-toggle.component.scss'],
  animations: [
    trigger('themeSwitch', [
      state('light', style({
        transform: 'rotate(-180deg)'
      })),
      state('dark', style({
        transform: 'rotate(0deg)'
      })),
      transition('light <=> dark', animate('500ms ease-in-out'))
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeToggleComponent {

  public readonly icon$: Observable<ThemeIcon>;
  public readonly animationState$: Observable<AnimationState>

  constructor(
    private readonly _theme: ThemeService
  ) {
    this.icon$ = this._theme.themeChanges$.pipe(
      map(theme => theme === 'light' ? 'light_mode' : 'dark_mode')
    );
    this.animationState$ = this._theme.themeChanges$;
  }

  public toggleTheme(): void {
    this._theme.toggle();
  }

}
