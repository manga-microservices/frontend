import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule, TransferState } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule as AppLayoutModule } from './modules/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { LayoutModule } from '@angular/cdk/layout';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { browserTranslateLoaderFactory, WarnMissingTranslationService, LocalizedTitleStrategy } from './core/localization';
import { GraphQLModule } from './modules/graphql';
import { SsrCookieService } from 'ngx-cookie-service-ssr';
import { AVAILABLE_LANGUAGES, LANGUAGE_STORAGE, LanguageService, THEME_STORAGE, ThemeService } from './services';
import { CookieLanguageStorage, CookieThemeStorage } from './core/models';
import { MenuModule } from './modules/menu';
import { TitleStrategy } from '@angular/router';

function initializeApp(
  themeService: ThemeService,
  langService: LanguageService
): () => void {
  return () => {
    themeService.initPersistedTheme();
    langService.initPersistedLang();
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,

    AppRoutingModule,
    GraphQLModule,
    AppLayoutModule,
    MenuModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: browserTranslateLoaderFactory,
        deps: [HttpClient, TransferState]
      },
      defaultLanguage: 'en',
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: WarnMissingTranslationService },
    }),

    LayoutModule,
    MatSidenavModule,
  ],
  providers: [
    SsrCookieService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ThemeService, LanguageService],
      multi: true
    },
    { provide: TitleStrategy, useClass: LocalizedTitleStrategy },
    {
      provide: THEME_STORAGE,
      useClass: CookieThemeStorage,
      deps: [SsrCookieService]
    },
    {
      provide: LANGUAGE_STORAGE,
      useClass: CookieLanguageStorage,
      deps: [SsrCookieService]
    },
    {
      provide: AVAILABLE_LANGUAGES,
      useValue: new Map([
        ['en', { code: 'en', icon: 'gb', fullName: 'English' }]
      ])
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
