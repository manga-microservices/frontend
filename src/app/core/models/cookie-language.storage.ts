import { SsrCookieService } from 'ngx-cookie-service-ssr';
import { LanguageStorage } from 'app/services';

export const LANGUAGE_COOKIE_NAME = 'lang';

export class CookieLanguageStorage implements LanguageStorage {

  constructor(
    private readonly _cookieService: SsrCookieService
  ) {
  }

  public get(): string | null {
    const lang =  this._cookieService.get(LANGUAGE_COOKIE_NAME);
    return lang.length === 0 ? null : lang;
  }

  public set(lang: string): void {
    return this._cookieService.set(LANGUAGE_COOKIE_NAME, lang);
  }

}
