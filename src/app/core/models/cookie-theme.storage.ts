import { SsrCookieService } from 'ngx-cookie-service-ssr';
import { Theme, ThemeStorage } from 'app/services';

export const THEME_COOKIE_NAME = 'theme';

export class CookieThemeStorage implements ThemeStorage {

  constructor(
    private readonly _cookieService: SsrCookieService
  ) {
  }

  get(): Theme | null {
    const theme = this._cookieService.get(THEME_COOKIE_NAME) as Theme;
    return theme.length === 0 ? null : theme;
  }

  set(theme: Theme): void {
    return this._cookieService.set(THEME_COOKIE_NAME, theme);
  }
}
