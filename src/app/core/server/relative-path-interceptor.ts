import { Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { REQUEST } from '@nguniversal/express-engine/tokens'
import { Request } from 'express';
import { Observable } from 'rxjs';

function isAbsoluteUrl(url: string) {
  return url.startsWith('http') || url.startsWith('//');
}

@Injectable()
export class RelativePathInterceptor implements HttpInterceptor {

  constructor(
    @Inject(REQUEST) private readonly _request: Request
  ) {
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this._request && !isAbsoluteUrl(req.url)) {
      const protocolHost = `${this._request.protocol}://${this._request.get(
        'host'
      )}`;

      const pathSeparator = !req.url.startsWith('/') ? '/' : '';
      const url = protocolHost + pathSeparator + req.url;
      const serverRequest = req.clone({ url });

      return next.handle(serverRequest);
    }

    return next.handle(req);
  }
}
