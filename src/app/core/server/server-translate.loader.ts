import { TranslateLoader } from '@ngx-translate/core';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';
import { Observable, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export class ServerTranslateLoader implements TranslateLoader {
  constructor(
    private readonly _http: HttpClient,
    private readonly _transferState: TransferState
  ) {
  }

  public getTranslation(lang: string): Observable<any> {
    return new TranslateHttpLoader(this._http).getTranslation(lang).pipe(
      tap(jsonData => {
        const key: StateKey<number> = makeStateKey<number>(
          'transfer-translate-' + lang
        );
        this._transferState.set(key, jsonData);
      })
    );
  }
}

export function serverTranslateLoaderFactory(
  http: HttpClient,
  transferState: TransferState
) {
  return new ServerTranslateLoader(http, transferState);
}
