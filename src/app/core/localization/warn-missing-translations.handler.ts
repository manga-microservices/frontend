import { MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';

export class WarnMissingTranslationService implements MissingTranslationHandler {
  public handle(params: MissingTranslationHandlerParams): string {
    return `WARN: '${params.key}' is missing in '${params.translateService.currentLang}' locale`;
  }
}
