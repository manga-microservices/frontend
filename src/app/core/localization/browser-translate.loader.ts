import { TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export class BrowserTranslateLoader implements TranslateLoader {

  constructor(
    private readonly _http: HttpClient,
    private readonly _transferState: TransferState
  ) {
  }

  public getTranslation(lang: string): Observable<any> {
    const key: StateKey<number> = makeStateKey<number>(
      'transfer-translate-' + lang
    );

    const data = this._transferState.get(key, null);
    if (data != null) {
      return new Observable((observer) => {
        observer.next(data);
        observer.complete();
      });
    } else {
      return new TranslateHttpLoader(this._http).getTranslation(lang);
    }
  }
}

export function browserTranslateLoaderFactory(
  httpClient: HttpClient,
  transferState: TransferState
) {
  return new BrowserTranslateLoader(httpClient, transferState);
}
