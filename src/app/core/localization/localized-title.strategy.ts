import { Injectable, OnDestroy } from '@angular/core';
import { RouterStateSnapshot, TitleStrategy } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { Subject, takeUntil } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LocalizedTitleStrategy extends TitleStrategy implements OnDestroy {

  private readonly _destroyed = new Subject<void>();

  constructor(
    private readonly _translateService: TranslateService,
    private readonly _title: Title
  ) {
    super();
  }

  public ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }

  public updateTitle(snapshot: RouterStateSnapshot): void {
    const titleKey = this.buildTitle(snapshot);
    if (titleKey == null) {
      return
    }

    this._translateService.get(titleKey)
      .pipe(takeUntil(this._destroyed))
      .subscribe(title => this._title.setTitle(`${title} | MoeManga`));
  }
}
