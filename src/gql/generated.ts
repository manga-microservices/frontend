export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Date with time (isoformat) */
  DateTime: any;
  Upload: any;
};

export type EntityAlreadyExistsError = Error & {
  __typename?: 'EntityAlreadyExistsError';
  message: Scalars['String'];
};

export type Error = {
  message: Scalars['String'];
};

export enum Language {
  Deu = 'deu',
  Eng = 'eng',
  Rus = 'rus',
  Ukr = 'ukr'
}

export type Manga = {
  __typename?: 'Manga';
  chapters: MangaChapterPagePaginationResult;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  infos: Array<MangaInfo>;
  tags: Array<MangaTag>;
  title: Scalars['String'];
  titleSlug: Scalars['String'];
  updatedAt: Scalars['DateTime'];
};


export type MangaChaptersArgs = {
  page?: Scalars['Int'];
  pageSize?: Scalars['Int'];
};


export type MangaInfosArgs = {
  preferredLanguages?: InputMaybe<Array<Language>>;
};

export type MangaChapter = {
  __typename?: 'MangaChapter';
  createdBy: User;
  id: Scalars['ID'];
  number: Scalars['String'];
  pages: Array<MangaPage>;
  title: Scalars['String'];
  volume?: Maybe<Scalars['Int']>;
};

export type MangaChapterCreateInput = {
  branchId: Scalars['ID'];
  number: Scalars['String'];
  title?: InputMaybe<Scalars['String']>;
  volume?: InputMaybe<Scalars['Int']>;
};

export type MangaChapterPagePaginationResult = {
  __typename?: 'MangaChapterPagePaginationResult';
  items: Array<MangaChapter>;
  pageInfo: PagePaginationInfo;
};

export type MangaCreateErrors = ValidationErrors;

export type MangaCreateInput = {
  title: Scalars['String'];
};

export type MangaCreatePayload = {
  __typename?: 'MangaCreatePayload';
  errors: Array<MangaCreateErrors>;
  manga?: Maybe<Manga>;
};

export type MangaGetApprovalQueueResult = {
  __typename?: 'MangaGetApprovalQueueResult';
  errors?: Maybe<Array<Error>>;
  result?: Maybe<Array<Manga>>;
};

export type MangaGroup = {
  __typename?: 'MangaGroup';
  createdAt: Scalars['DateTime'];
  createdBy: User;
  id: Scalars['ID'];
  members: Array<User>;
  title: Scalars['String'];
};

export type MangaInfo = {
  __typename?: 'MangaInfo';
  description: Scalars['String'];
  id: Scalars['ID'];
  language: Language;
  title: Scalars['String'];
};

export type MangaPage = {
  __typename?: 'MangaPage';
  id: Scalars['ID'];
  imageUrl: Scalars['String'];
  number: Scalars['Int'];
};

export type MangaPagePaginationResult = {
  __typename?: 'MangaPagePaginationResult';
  items: Array<Manga>;
  pageInfo: PagePaginationInfo;
};

export type MangaSearchInput = {
  searchTerm?: InputMaybe<Scalars['String']>;
  tagsExclude?: InputMaybe<Array<Scalars['String']>>;
  tagsInclude?: InputMaybe<Array<Scalars['String']>>;
};

export type MangaTag = {
  __typename?: 'MangaTag';
  id: Scalars['ID'];
  name: Scalars['String'];
  nameSlug: Scalars['String'];
};

export type MangaTagCreateErrors = EntityAlreadyExistsError | ValidationErrors;

export type MangaTagCreateInput = {
  name: Scalars['String'];
};

export type MangaTagCreatePayload = {
  __typename?: 'MangaTagCreatePayload';
  errors: Array<MangaTagCreateErrors>;
  tag?: Maybe<MangaTag>;
};

export type Mutation = {
  __typename?: 'Mutation';
  mangaChapterCreate?: Maybe<MangaChapter>;
  mangaCreate: MangaCreatePayload;
  mangaTagCreate: MangaTagCreatePayload;
  userCreate: UserCreateResult;
};


export type MutationMangaChapterCreateArgs = {
  images: Array<Scalars['Upload']>;
  input: MangaChapterCreateInput;
};


export type MutationMangaCreateArgs = {
  input: MangaCreateInput;
};


export type MutationMangaTagCreateArgs = {
  input: MangaTagCreateInput;
};


export type MutationUserCreateArgs = {
  input: UserCreateInput;
};

export type PagePaginationInfo = {
  __typename?: 'PagePaginationInfo';
  currentPage: Scalars['Int'];
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  pageSize: Scalars['Int'];
  totalItems: Scalars['Int'];
  totalPages: Scalars['Int'];
};

export type PagePaginationInput = {
  page?: InputMaybe<Scalars['Int']>;
  pageSize?: InputMaybe<Scalars['Int']>;
};

export type PermissionError = Error & {
  __typename?: 'PermissionError';
  message: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  getUser?: Maybe<User>;
  mangaChapterGet?: Maybe<MangaChapter>;
  mangaChaptersRecent: Array<MangaChapter>;
  mangaGet?: Maybe<Manga>;
  mangaGetApprovalQueue: MangaGetApprovalQueueResult;
  mangaGroupGet?: Maybe<MangaGroup>;
  mangaSearch: MangaPagePaginationResult;
  mangaTags: Array<MangaTag>;
  me?: Maybe<User>;
};


export type QueryGetUserArgs = {
  id: Scalars['ID'];
};


export type QueryMangaChapterGetArgs = {
  id: Scalars['ID'];
};


export type QueryMangaChaptersRecentArgs = {
  languages?: InputMaybe<Array<Language>>;
};


export type QueryMangaGetArgs = {
  id?: InputMaybe<Scalars['ID']>;
  titleSlug?: InputMaybe<Scalars['String']>;
};


export type QueryMangaGroupGetArgs = {
  id: Scalars['ID'];
};


export type QueryMangaSearchArgs = {
  input: MangaSearchInput;
  pagination: PagePaginationInput;
};

export type User = {
  __typename?: 'User';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  username: Scalars['String'];
};

export type UserCreateInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type UserCreateResult = {
  __typename?: 'UserCreateResult';
  user?: Maybe<User>;
};

export type ValidationError = Error & {
  __typename?: 'ValidationError';
  code: Scalars['String'];
  location: Array<Scalars['String']>;
  message: Scalars['String'];
};

export type ValidationErrors = Error & {
  __typename?: 'ValidationErrors';
  errors: Array<ValidationError>;
  message: Scalars['String'];
};

export type MangaGetByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MangaGetByIdQuery = { __typename?: 'Query', mangaGet?: { __typename?: 'Manga', id: string, title: string, titleSlug: string, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, nameSlug: string }> } | null };
